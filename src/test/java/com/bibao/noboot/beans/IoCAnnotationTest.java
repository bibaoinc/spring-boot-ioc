package com.bibao.noboot.beans;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bibao.noboot.config.BeanConfig;

public class IoCAnnotationTest {
	private AnnotationConfigApplicationContext actx;
	
	@Before
	public void setUp() throws Exception {
		actx = new AnnotationConfigApplicationContext(BeanConfig.class);
	}

	@After
	public void tearDown() throws Exception {
		actx.close();
	}

	@Test
	public void testEmployee() {
		Employee emp = actx.getBean("emp", Employee.class);
		assertEquals(2, emp.getId());
		assertEquals("Bob", emp.getName());
		Employee emp2 = actx.getBean("emp", Employee.class);
		assertNotSame(emp, emp2);
		assertSame(emp.getName(), emp2.getName());
	}

	@Test
	public void testPerson() {
		Person person = actx.getBean("person", Person.class);
		assertEquals("Bruce", person.getFirstName());
		assertEquals("Lee", person.getLastName());
	}
	
	@Test
	public void testStudent() {
		Student student = actx.getBean("student", Student.class);
		assertEquals("Jason", student.getName());
		Address address = student.getLocation();
		assertEquals("New York City", address.getCity());
		assertEquals("NY", address.getState());
		Address address2 = actx.getBean("address", Address.class);
		assertSame(address, address2);
	}
	
}
