package com.bibao.noboot.beans;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IocXmlTest {
	private ClassPathXmlApplicationContext actx;
	
	@Before
	public void setUp() throws Exception {
		actx = new ClassPathXmlApplicationContext("config.xml");
	}

	@After
	public void tearDown() throws Exception {
		actx.close();
	}

	@Test
	public void testEmployee() {
		//Employee emp = (Employee)actx.getBean("emp");
		Employee emp = actx.getBean("emp", Employee.class);
		assertEquals(1, emp.getId());
		assertEquals("Alice", emp.getName());
		Employee emp2 = actx.getBean("emp", Employee.class);
		assertNotSame(emp, emp2);
		assertSame(emp.getName(), emp2.getName());
	}

	@Test
	public void testPerson() {
		Person person = actx.getBean("person", Person.class);
		assertEquals("Donald", person.getFirstName());
		assertEquals("Trump", person.getLastName());
	}
	
	@Test
	public void testStudent() {
		Student student = actx.getBean("student", Student.class);
		assertEquals("David", student.getName());
		Address address = student.getLocation();
		assertEquals("Princeton", address.getCity());
		assertEquals("NJ", address.getState());
	}
	
	@Test
	public void testAutowire() {
		Student student2 = actx.getBean("student2", Student.class);
		assertEquals("Tommy", student2.getName());
		assertNull(student2.getLocation());
		Student student3 = actx.getBean("student3", Student.class);
		assertEquals("Mary", student3.getName());
		assertNotNull(student3.getLocation());
		Address address = student3.getLocation();
		assertEquals("Princeton", address.getCity());
		assertEquals("NJ", address.getState());
	}
}
