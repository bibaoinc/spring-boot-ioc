package com.bibao.boot.beans;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IoCStereotypeXmlTest {
	private ClassPathXmlApplicationContext actx;
	
	@Before
	public void setUp() throws Exception {
		actx = new ClassPathXmlApplicationContext("contextconfig.xml");
	}

	@After
	public void tearDown() throws Exception {
		actx.close();
	}

	@Test
	public void testEmployee() {
		Employee emp = actx.getBean("emp", Employee.class);
		assertEquals(3, emp.getId());
		assertEquals("Robert", emp.getName());
		Employee emp2 = actx.getBean("emp", Employee.class);
		assertNotSame(emp, emp2);
		assertSame(emp.getName(), emp2.getName());
	}

	@Test
	public void testStudent() {
		Student student = actx.getBean("student", Student.class);
		assertEquals("Jack", student.getName());
		Address address = student.getLocation();
		assertEquals("MD", address.getState());
		assertEquals("Rockville", address.getCity());
	}
}
