package com.bibao.boot.beans;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.noboot.config.StereotypeBeanConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {StereotypeBeanConfig.class})
public class IoCNoBootTest {
	@Autowired
	private Employee emp;
	
	@Autowired
	private Student student;
	
	@Autowired
	private Stock stock;
	
	@Autowired
	private Customer customer;
	
	@Autowired
	private String username;
	
	@Autowired
	private String password;
	
	@Test
	public void testEmployee() {
		assertEquals(3, emp.getId());
		assertEquals("Robert", emp.getName());
	}

	@Test
	public void testStudent() {
		assertEquals("Jack", student.getName());
		Address address = student.getLocation();
		assertTrue(address instanceof DetailedAddress);
		assertEquals("MD", address.getState());
		assertEquals("Rockville", address.getCity());
		assertEquals("12345", ((DetailedAddress)address).getZipCode());
	}

	@Test
	public void testStock() {
		assertEquals("JPM", stock.getId());
		assertEquals("JP Morgan Chase", stock.getName());
	}
	
	@Test
	public void testEnvironment() {
		assertEquals("bibao", username);
		assertEquals("abcd1234", password);
	}
	
	@Test
	public void testCustomer() {
		assertEquals("Alex", customer.getName());
		assertFalse(CollectionUtils.isEmpty(customer.getEmails()));
		List<String> emails = customer.getEmails();
		assertEquals(2, emails.size());
		assertEquals("alex@gmail.com", emails.get(0));
		assertEquals("alex@yahoo.com", emails.get(1));
	}
}
