package com.bibao.boot.springbootioc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.bibao.boot"})
public class SpringBootIocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootIocApplication.class, args);
	}
}
