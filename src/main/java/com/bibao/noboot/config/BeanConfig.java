package com.bibao.noboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.bibao.noboot.beans.Address;
import com.bibao.noboot.beans.Employee;
import com.bibao.noboot.beans.Person;
import com.bibao.noboot.beans.Student;

@Configuration
public class BeanConfig {
	
	@Bean("emp")
	@Scope("prototype")
	public Employee employee() {
		Employee emp = new Employee();
		emp.setId(2);
		emp.setName("Bob");
		return emp;
	}
	
	@Bean
	public Person person() {
		return new Person("Bruce", "Lee");
	}
	
	@Bean
	public Address address() {
		Address address = new Address();
		address.setCity("New York City");
		address.setState("NY");
		return address;
	}
	
	@Bean
	public Student student() {
		Student student = new Student();
		student.setName("Jason");
		student.setLocation(address());
		return student;
	}
	
}
