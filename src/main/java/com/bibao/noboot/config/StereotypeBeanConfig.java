package com.bibao.noboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = {"com.bibao.boot.beans"})
@PropertySource("classpath:application.properties")
public class StereotypeBeanConfig {
	@Autowired
	private Environment env;
	
	@Bean
	public String username() {
		return env.getProperty("env.username");
	}
	
	@Bean
	public String password() {
		return env.getProperty("env.password");
	}
}
